import React from 'react'
import "../video/Video.css"
import Playvideo from '../../components/playvideo/Playvideo'
import Recommened from '../../components/Recommended/Recommened'
import { useParams } from 'react-router-dom'
const Video = () => {
  const {videoId,categoryId}=useParams();
  return (
    <div className='play-container'>
      <Playvideo videoId={videoId} categoryId={categoryId}/>
      <Recommened categoryId={categoryId}/>

    </div>
  )
}

export default Video