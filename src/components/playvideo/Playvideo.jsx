import React, { useEffect, useState } from 'react';
import "../playvideo/Playvideo.css";
import moment from 'moment';
import like from "../../assets/like.png";
import dislike from "../../assets/dislike.png";
import share from "../../assets/share.png";
import save from "../../assets/save.png";
import { value_converter } from '../../data';
import { useParams } from 'react-router-dom';

const Playvideo = () => {
    const {videoId}=useParams()
    // API key
    const API_KEY = import.meta.env.VITE_API_KEY;
    const [apiData, setApiData] = useState(null);
    const [channelData, setChannelData] = useState(null)
    const [commentData, setcommentData] = useState([])

    const fetchVideoData = async () => {
        try {
            // Fetching video data
            const videoDetails_url = `https://youtube.googleapis.com/youtube/v3/videos?part=snippet%2Cstatistics&id=${videoId}&key=${API_KEY}`;
            const response = await fetch(videoDetails_url);
            const data = await response.json();
            // console.log("Video Data:", data);
            setApiData(data.items[0]);
        } catch (error) {
            console.error("Error fetching video data:", error);
        }
    };
    //

    const fetchOtherData = async () => {
        try {
            // Fetching channel data
            const channelDetails_url = `https://youtube.googleapis.com/youtube/v3/channels?part=snippet%2CcontentDetails%2Cstatistics&id=${apiData.snippet.channelId}&key=${API_KEY}`
            const response = await fetch(channelDetails_url);
            const data = await response.json();
            //console.log("Channel Data:", data);
            setChannelData(data.items[0]);

            //fetch comment data
            const comment_url = `https://youtube.googleapis.com/youtube/v3/commentThreads?part=snippet%2Creplies&maxResults=50&videoId=${videoId}&key=${API_KEY}`
            const c_response = await fetch(comment_url)
            const c_data = await c_response.json()
            //console.log("comment Data:", c_data);
            setcommentData(c_data.items)
        }
        catch (error) {
            console.error("Error fetching channel data:", error);
        }
    };

    useEffect(() => {
        fetchVideoData();
    }, [videoId]);

    useEffect(() => {
        fetchOtherData();
    }, [apiData]);



    return (
        <div className='play-video'>
            <iframe
                src={`https://www.youtube.com/embed/${videoId}?autoplay=1`}
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                referrerpolicy="strict-origin-when-cross-origin"
                allowfullscreen
            ></iframe>
            <h3>{apiData ? apiData.snippet.title : "Title Here"}</h3>
            <div className='play-video-info'>
                <p>{apiData ? value_converter(apiData.statistics.viewCount) : "17k"} views &bull; {apiData ? moment(apiData.snippet.publishedAt).fromNow() : ""}</p>
                <div>
                    <span><img src={like} alt='' />{apiData ? value_converter(apiData.statistics.likeCount) : ""}</span>
                    <span><img src={dislike} alt='' /></span>
                    <span><img src={share} alt='' />share</span>
                    <span><img src={save} alt='' />save</span>
                </div>
            </div>
            <hr />
            <div className='publisher'>
                <img src={channelData ? channelData.snippet.thumbnails.default.url : ""} alt='' />
                <div>
                    <p>{apiData ? apiData.snippet.channelTitle : ""}</p>
                    <span>{channelData ? value_converter(channelData.statistics.subscriberCount) : "10"} Subscribers</span>
                </div>
                <button>Subscribe</button>
            </div>
            <div className='vid-description'>
                <p>{apiData ? apiData.snippet.description.slice(0, 250) : "Description here"}</p>

                <hr />
                <h4>{apiData ? value_converter(apiData.statistics.commentCount) : "100"} comments</h4>

                {
                    commentData.map((item, index) => {
                        return (
                            <div key={index} className='comment'>
                                <img src={item.snippet.topLevelComment.snippet.authorProfileImageUrl} alt='' />
                                <div>
                                    <h3>{item.snippet.topLevelComment.snippet.authorDisplayName}<span>1 day ago</span></h3>
                                    <p>{item.snippet.topLevelComment.snippet.textDisplay}</p>
                                    <div className='comment-action'>
                                        <img src={like} alt='' /><span>{value_converter(item.snippet.topLevelComment.snippet.likeCount)}</span>
                                        <img src={like} alt='' /><span>233</span>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }

            </div>
        </div>
    );
};

export default Playvideo;
