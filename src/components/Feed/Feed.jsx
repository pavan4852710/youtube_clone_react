import React, { useEffect, useState } from 'react';
//import { API_KEY } from '../../data';
import { Link } from 'react-router-dom';
import '../Feed/Feed.css';
import { value_converter } from '../../data';
import moment from 'moment';
const Feed = ({ category }) => {
    
    //api key from .env
    const API_KEY = import.meta.env.VITE_API_KEY;

    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);

    const fetchData = async () => {
        try {
            const videoList_url = `https://youtube.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&chart=mostPopular&maxResults=50&regionCode=US&videoCategoryId=${category}&key=${API_KEY}`;
            const response = await fetch(videoList_url);
            const result = await response.json();
            setData(result.items);
            //console.log(result.items)
            setLoading(false);
        } catch (error) {
            console.error('Error fetching data:', error);
            // Handle error state here
            setLoading(false);
        }
    };

    useEffect(() => {
        fetchData();
    }, [category]);

    return (
        <div className="feed">
        {loading ? (
            <p>Loading...</p>
        ) : (
            data.map((item, index) => (
                <Link key={item.id} to={`video/${item.snippet.categoryId}/${item.id}`} className="card">
                    <img src={item.snippet.thumbnails.medium.url} alt="thumbnail" />
                    <h2>{item.snippet.title}</h2>
                    <h3>{item.channelTitle}</h3>
                    <p>{value_converter(item.statistics.viewCount)} &bull; {moment(item.snippet.publishedAt).fromNow()}</p>
                </Link>
            ))
        )}
    </div>
    );
};

export default Feed;
